package com.innovationlabs.gateway.resolvers;

import com.innovationlabs.gateway.service.ServiceAService;
import com.innovationlabs.gateway.service.ServiceBService;
import com.innovationlabs.lib.AppInfoReply;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-07
 * Time: 14:57
 */
class QueryResolverTest {

    @Mock
    ServiceAService aService;

    @Mock
    ServiceBService bService;

    @InjectMocks
    QueryResolver resolver;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        when(aService.getAppInformationForService())
                .thenReturn(serviceAInfo());

        when(bService.getAppInformationForService())
                .thenReturn(serviceBInfo());
    }

    @Test
    void connectedAppInfo() {
        var appFullInfoList = resolver.connectedAppInfo();

        var serviceAInfo = serviceAInfo();

        verify(aService, times(1))
                .getAppInformationForService();

        verify(bService, times(1))
                .getAppInformationForService();

        var serviceAFullInfo = appFullInfoList.get(0);
        assertAll(
                () -> assertEquals(2, appFullInfoList.size()),
                () -> assertEquals(serviceAInfo.getAppName(), serviceAFullInfo.getAppName()),
                () -> assertEquals(serviceAInfo.getBuildVersion(), serviceAFullInfo.getBuildVersion()),
                () -> assertEquals(serviceAInfo.getBranch(), serviceAFullInfo.getBranch()),
                () -> assertEquals(serviceAInfo.getLastBuildTime(), serviceAFullInfo.getLastBuildTime()),
                () -> assertEquals(serviceAInfo.getCommitTime(), serviceAFullInfo.getCommitTime()),
                () -> assertEquals(serviceAInfo.getName(), serviceAFullInfo.getAuthor().getName()),
                () -> assertEquals(serviceAInfo.getEmail(), serviceAFullInfo.getAuthor().getEmail())
        );
    }

    @Test
    void authors() {
        var authorList = resolver.authors();
        var serviceAInfo = serviceAInfo();

        verify(aService, times(1))
                .getAppInformationForService();

        verify(bService, times(1))
                .getAppInformationForService();

        var serviceAAuthor = authorList.get(0);
        assertAll(
                () -> assertEquals(2, authorList.size()),
                () -> assertEquals(serviceAAuthor.getEmail(), serviceAInfo.getEmail()),
                () -> assertEquals(serviceAAuthor.getName(), serviceAInfo.getName())
        );
    }

    AppInfoReply serviceAInfo() {
        return AppInfoReply.newBuilder()
                .setAppName("service-a")
                .setCommitTime("2021-11-07T07:56:04+0100")
                .setBranch("master")
                .setLastBuildTime("2021-11-07T14:18:03+0100")
                .setEmail("test@email.com")
                .setName("Tester")
                .build();
    }

    AppInfoReply serviceBInfo() {
        return AppInfoReply.newBuilder()
                .setAppName("service-b")
                .setCommitTime("2021-11-07T07:56:04+0100")
                .setBranch("master")
                .setLastBuildTime("2021-11-07T14:18:03+0100")
                .setEmail("test@email.com")
                .setName("Tester")
                .build();
    }
}