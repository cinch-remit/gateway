package com.innovationlabs.gateway.service;

import com.innovationlabs.lib.AppInfoReply;
import com.innovationlabs.lib.AppInfoRequest;
import com.innovationlabs.lib.AppInfoServiceGrpc;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-07
 * Time: 14:35
 */
class ServiceBServiceTest {

    @Mock
    AppInfoServiceGrpc.AppInfoServiceBlockingStub appInfoServiceBlockingStub;

    @InjectMocks
    ServiceBService bService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
    }

    @Test
    void getAppInformationForService() {
        when(appInfoServiceBlockingStub.getAppInfo(any(AppInfoRequest.class)))
                .thenReturn(AppInfoReply.newBuilder().build());

        var reply = bService.getAppInformationForService();
        assertNotNull(reply);
        verify(appInfoServiceBlockingStub, times(1))
                .getAppInfo(any(AppInfoRequest.class));
    }
}