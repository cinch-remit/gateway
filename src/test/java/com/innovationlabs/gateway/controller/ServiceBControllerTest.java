package com.innovationlabs.gateway.controller;

import com.innovationlabs.gateway.service.ServiceBService;
import com.innovationlabs.lib.AppInfoReply;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-07
 * Time: 14:51
 */
class ServiceBControllerTest {

    @Mock
    ServiceBService service;

    @InjectMocks
    ServiceBController controller;

    MockMvc mockMvc;

    String endpoint = "/service-b";

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }

    @Test
    void getAppInfo() throws Exception {
        var appName = "test app";
        var currentBranch = "master";
        var buildVersion = "3489DJ";

        var appInfoReply = AppInfoReply.newBuilder()
                .setAppName(appName)
                .setBranch(currentBranch)
                .setBuildVersion(buildVersion)
                .build();

        when(service.getAppInformationForService())
                .thenReturn(appInfoReply);

        mockMvc.perform(get(endpoint))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.appName", is(appName)))
                .andExpect(jsonPath("$.currentBranch", is(currentBranch)))
                .andExpect(jsonPath("$.buildVersion", is(buildVersion)));
    }
}