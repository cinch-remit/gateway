package com.innovationlabs.gateway.resolvers;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import com.innovationlabs.gateway.model.AppFullInfo;
import com.innovationlabs.gateway.model.Author;
import com.innovationlabs.gateway.service.ServiceAService;
import com.innovationlabs.gateway.service.ServiceBService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 17:25
 */

@Service
@Slf4j
@Profile("!test")
public class QueryResolver implements GraphQLQueryResolver {
    private final ServiceAService aService;
    private final ServiceBService bService;

    public QueryResolver(ServiceAService aService, ServiceBService bService) {
        this.aService = aService;
        this.bService = bService;
    }

    /**
     * Get information on connected applications.
     * @return A list of information for all connected apps {@link List<AppFullInfo>}.
     */
    public List<AppFullInfo> connectedAppInfo() {
        log.info("Requesting app information via graphQL");
        var serviceAInfo = aService.getAppInformationForService();
        var serviceBInfo = bService.getAppInformationForService();

        return List.of(new AppFullInfo(serviceAInfo), new AppFullInfo(serviceBInfo));
    }

    /**
     * Get git commit authors for current version of running connecting app.
     * @return A list of authors for all connected apps {@link List<Author>}.
     */
    public List<Author> authors() {
        log.info("Requesting author information via graphQL");
        var serviceAInfo = aService.getAppInformationForService();
        var serviceBInfo = bService.getAppInformationForService();

        return List.of(new Author(serviceAInfo), new Author(serviceBInfo));
    }
}
