package com.innovationlabs.gateway.controller;

import com.innovationlabs.gateway.service.ServiceBService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 23:31
 */
@RestController
public class ServiceBController {
    private final ServiceBService service;

    public ServiceBController(ServiceBService service) {
        this.service = service;
    }

    @RequestMapping("/service-b")
    public Map<String, String> getAppInfo() {
        var reply = service.getAppInformationForService();
        return Map.of("appName",reply.getAppName(), "currentBranch", reply.getBranch(), "buildVersion", reply.getBuildVersion());
    }
}
