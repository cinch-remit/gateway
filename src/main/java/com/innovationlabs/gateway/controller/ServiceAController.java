package com.innovationlabs.gateway.controller;

import com.innovationlabs.gateway.service.ServiceAService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 23:31
 */
@RestController
public class ServiceAController {
    private final ServiceAService service;

    public ServiceAController(ServiceAService service) {
        this.service = service;
    }

    @RequestMapping("/service-a")
    public Map<String, String> getAppInfo() {
        var reply = service.getAppInformationForService();
        return Map.of("appName",reply.getAppName(), "currentBranch", reply.getBranch(), "buildVersion", reply.getBuildVersion());
    }
}
