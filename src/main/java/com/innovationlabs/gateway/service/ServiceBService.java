package com.innovationlabs.gateway.service;


import com.innovationlabs.lib.AppInfoReply;
import com.innovationlabs.lib.AppInfoRequest;
import com.innovationlabs.lib.AppInfoServiceGrpc;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.client.inject.GrpcClient;
import org.springframework.stereotype.Service;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 23:23
 */
@Service
@Slf4j
public class ServiceBService implements InformationService{

    @GrpcClient("service-b")
    private AppInfoServiceGrpc.AppInfoServiceBlockingStub appInfoServiceBlockingStub;

    /**
     * Get app information for service-b
     * @return App iformation for listed app {@link AppInfoReply}
     */
    @Override
    public AppInfoReply getAppInformationForService() {
        var request = AppInfoRequest.newBuilder().build();
        return appInfoServiceBlockingStub.getAppInfo(request);
    }
}
