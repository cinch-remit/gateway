package com.innovationlabs.gateway.service;

import com.innovationlabs.lib.AppInfoReply;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 23:34
 */
public interface InformationService {
    AppInfoReply getAppInformationForService();
}
