package com.innovationlabs.gateway.logging;

import io.grpc.*;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 21:55
 */
@Slf4j
public class GrpcLogInterceptor implements ClientInterceptor {

    @Override
    public <ReqT, RespT> ClientCall<ReqT, RespT> interceptCall(MethodDescriptor<ReqT, RespT> methodDescriptor,
                                                               CallOptions callOptions, Channel channel) {
        log.info("client call from method: {}",methodDescriptor.getFullMethodName());
        return channel.newCall(methodDescriptor, callOptions);
    }
}
