package com.innovationlabs.gateway.logging;

import net.devh.boot.grpc.client.interceptor.GrpcGlobalClientInterceptor;
import org.springframework.context.annotation.Configuration;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-05
 * Time: 21:57
 */
@Configuration
public class GrpcLoggingConfiguration {

    @GrpcGlobalClientInterceptor
    GrpcLogInterceptor logInterceptor() {
        return new GrpcLogInterceptor();
    }
}
