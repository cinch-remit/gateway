package com.innovationlabs.gateway.model;

import com.innovationlabs.lib.AppInfoReply;
import lombok.Data;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 16:04
 */
@Data
public class Author {
    private String name;
    private String email;

    public Author(AppInfoReply appInfoReply) {
        this.name = appInfoReply.getName();
        this.email = appInfoReply.getEmail();
    }
}
