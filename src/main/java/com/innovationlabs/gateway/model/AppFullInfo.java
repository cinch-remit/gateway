package com.innovationlabs.gateway.model;

import com.innovationlabs.lib.AppInfoReply;
import lombok.Data;

/**
 * @author Robinson Mgbah
 * Date: 2021-11-06
 * Time: 16:04
 */
@Data
public class AppFullInfo {
    private String appName;
    private String buildVersion;
    private String branch;
    private String lastBuildTime;
    private String commitTime;
    private Author author;

    public AppFullInfo(AppInfoReply appInfoReply) {
        this.appName = appInfoReply.getAppName();
        this.buildVersion = appInfoReply.getBuildVersion();
        this.branch = appInfoReply.getBranch();
        this.lastBuildTime = appInfoReply.getLastBuildTime();
        this.commitTime = appInfoReply.getCommitTime();
        this.author = new Author(appInfoReply);
    }
}
