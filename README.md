# README #

## What is this repository for? ###

* `gateway` service gives access to microservices within the application. It provides one point of entry for the services in the application.
* current version `0.0.1-SNAPSHOT`
* container name `cinchremit/gateway`
* stable tag `master`

## How do I get set up? ###

* Install java, maven, docker and kubernetes
* There are 2 profiles which can be run `local-dev` & `default`.
* `local-dev` should be used when running in container based enviroments(`docker` or `kubernetes`).
* `default` should be used when developing locally.
* The interface dependency below is hosted on my private maven repository.  
```xml
<dependency>
  <groupId>com.innovationlabs</groupId>
  <artifactId>interface</artifactId>
  <version>1.0-SNAPSHOT</version>
</dependency>
```
Included in this project is a template file maven settings with the needed entries. You will just need to update the {{MAVEN_REPO_PASSWORD}} fields with the proper credentials and then point
your maven settings to this file in order to run this project locally with maven. The credentials were sent in the submission email. You can request them again by sending me an email: mgbahacho@aol.com
If you only intend to run through containers, then this step isn't necessary.
* If you decide to use the [start script](start-dependencies.sh) or [manual build script](manual-build.sh), it will most likely fail because of 2 reasons
- you haven't pointed in the right maven file(see [The point above on maven](#how-do-i-get-set-up))
To solve this, see the point above about maven interface dependency.
- you don't have access to the docker repository pointed in these files.
To solve this, you need to run the following steps
1. update to app name to match a docker repository you have access to. Change `app_name="cinchremit/gateway"` in the [manual build script](manual-build.sh) to the right docker repository name.
```
app_name="<right repository name>/gateway"
```
2. Update the `pod.spec.containers` section of the [workload-template-file](gateway-workload-template.yaml) to point to the right image
```yaml
containers:
        - envFrom:
          - configMapRef:
              name: gateway-config
          image: <right repository>/gateway:{{BRANCH}}
          name: gateway
```
3. If the image is `private` in your repository, you should change the settings to `public`.
4. If you'd rather not do that, then you need to create a secret and update the `pod.spec.imagePullSecrets` section of [workload-template-file](gateway-workload-template.yaml) with a secret containing access to your image repository.
- create a secret called reg-cred
```
### update the values placeholders with the real values.
kubectl create secret docker-registry regcred --docker-server=<your-registry-server> --docker-username=<your-name> --docker-password=<your-pword> --docker-email=<your-email> -n innovationlabs
```
- update the workload file
```yaml
spec:
  imagePullSecrets:
  - name: docker-regcred
  containers:
#rest of file omitted for brevity...
```
5. All set, you should be able to run [start script](start-dependencies.sh) or [manual build script](manual-build.sh) without issues.

#### Dependencies
* The following services are needed when running in a container environment.
    * `Elastic search` --> `port 9200`
    * `Kibana` --> `port 5601`
    * `Logstash` --> `port 5000`
    * `Service A` --> `ports 8080, 9090`
    * `Service B` --> `ports 8081, 9091`

## Running The Application
There are 3 simple ways to run all services.
1. Git clone this repo into the system/node you intend to run these files. You can apply the needed files. You can use the [start script](start-dependencies.sh) to deploy all files in the right order.
```
git clone https://cinch-remit@bitbucket.org/cinch-remit/k8s-manifests.git
cd k8s-manifests
sh start-dependencies.sh
```
2. If you'd rather not git clone the k8s repository, you could copy the contents of [deploy application script](https://bitbucket.org/cinch-remit/k8s-manifests/src/master/deploy-application-from-repo.sh) into a local file with `.sh` extension and run the script.(You should have kubernetes setup on your system).
```
cat <<EOF >localfile.sh
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
EOF 

sh localfile.sh
```
3. If you'd rather not create a file at all, you can just copy the text below directly into your terminal.
```
{
echo "delete previous namespace"
kubectl delete -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "creating namespace"
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/dev-namespace.yaml

echo "starting Elastic Logstash..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/elastic-search-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/kibana-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-config.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/elk/logstash-workload.yaml

echo "starting innovationlabs services..."
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-a-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/service-b-workload.yaml
kubectl apply -f https://bitbucket.org/cinch-remit/k8s-manifests/raw/7dd41c08a50f605ef7945824cb752495f0635ca2/localdev/innovationlabs/gateway-workload.yaml
}
```
### Running in local-dev profile
To run this application in a container environment, 
the manifest files from [k8s repository](https://bitbucket.org/cinch-remit/k8s-manifests/src/master/) should be used
to deploy this application.

Use the [start dependencies script](start-dependencies.sh) to start all services for `local-dev` environment.
* The [start dependencies script](start-dependencies.sh) generates a local workload file for your current branch and restarts all deployments. This script should be run if it is desired to push changes to repository before restarting the application.
```
git clone https://cinch-remit@bitbucket.org/cinch-remit/gateway.git
cd gateway
sh start-dependencies.sh
```

* If the container with the current branch already exists, the running [build script](manual-build.sh) will package and push your changes to dockerhub. You can then restart the gateway application using `kubectl rollout restart deployment gateway -n innovationlabs`
* The container is stored with docker repository with the name `cinchremit/gateway`. 
* This application runs on port 8082 but can be changed by adding the environment variable `server.port=<port number>` in the env section of the `gateway-workload.yaml`.
```yaml
env:
- name: server.port
  value: <port number>
```
Update the container ports section of the `gateway-workload.yaml` file
```yaml
ports:
          - containerPort: <port number>
            name: http
```
Update the ports section of the gateway-svc section as well in the `gateway-workload.yaml` file
```yaml
ports:
    - port: <port number>
      protocol: TCP
      targetPort: <port number>
```

A much easier way to change the port without having to change the deployment file is to update the gateway-svc targetPort section to the desired port
```yaml
ports:
    - port: 8082
      protocol: TCP
      targetPort: <port number>
```
### Running in default profile
You can run the application from your IDE or alternatively from the terminal using `mvn spring-boot:run`

### How to run tests
Use maven to run tests (`mvn test`)

## Endpoints.
### GraphQL
A graphQL endpoint exists on path `/graphql`. This endpoint retrieves information about the application including some git information.
A simple curl test can be run with the code below in a terminal or imported into postman and run from. Remember to change localhost:port to the appropriate value for your deployment.
```curl
curl --location --request POST 'http://localhost:8082/graphql' \
--header 'Content-Type: application/json' \
--data-raw '{"query":"{\n  connectedAppInfo {\n    appName\n    commitTime\n    branch\n    author {\n      email\n    }\n    lastBuildTime\n    commitTime\n  }\n}","variables":{}}'
```

There exists UI interface for graphql which can be found on path `/graphiql`. GraphQL queries can also be run in that interface..
A simple graphQl query can be run with the code below
```graphql
{
  connectedAppInfo {
    appName
    commitTime
    branch
    author {
      email
    }
    lastBuildTime
    commitTime
  }
}
```

### Grpc
This application is also a gateway for the other innovationlabs services(`serviceA` & `serviceB`). It exposes 2 endpoints from these services
which simple displays information about the service.
The communication between the internal services and the gateway is done via `grpc`.
* service a: 
``` 
curl --location --request GET 'http://localhost:8082/service-a'
```
Remember to change localhost:port to the appropriate value for your deployment.
* service b: 
``` 
curl --location --request GET 'http://localhost:8082/service-b'
```
Remember to change localhost:port to the appropriate value for your deployment.

## Logs
### local-dev
When running the container environment, logs can be monitored through the kibana interface which is found at path `http://localhost:5601/kibana`. Remember to change localhost:port to the appropriate value for your deployment.
By adding the `logstash-logback-encoder` in the [pom file](pom.xml), and configuring the [logback configuration](src/main/resources/logback.xml) to point to the logstash endpoint, we are able to send logs directly from our app to logstash.
```xml
<!--logstash-logback-encoder dependency-->
<dependency>
  <groupId>net.logstash.logback</groupId>
  <artifactId>logstash-logback-encoder</artifactId>
  <version>6.6</version>
</dependency>
```
```xml
<!-- logstash config part of logback.xml -->
<appender name="logstash" class="net.logstash.logback.appender.LogstashTcpSocketAppender">
  <destination>logstash_host:logstash_port</destination>
  <encoder class="net.logstash.logback.encoder.LogstashEncoder"/>
</appender>
```
This simplifies the process of sending logs through logstash to elastic-search.
### default
When running in default, logs can be seen through console.

## Improvements
Some ideas on how to improve this service. 
### Configuration Server
Place configurations for the application in a central place which can be accessed by the application at startup. 
The application does not need to be rebuilt for config changes. The application only needs to be restarted for changes to take effect. 
### Security(Authentication & Authorization)
The application at this moment is open and anyone can access it. By leveraging spring security the application can be secured in a way that only authenticated users can access the application and authenticated users can only perform actions they are authorized to do.
### Tracing
With microservices, calls will be made across various services, finding a way to track these calls can be of value for monitoring all systems.
A great tool is [zipkin](https://zipkin.io).
### Who do I talk to? ###

* Repo owner or admin
    * Robinson Mgbah - mgbahacho@aol.com

